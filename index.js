var PluginIO = require('origami-plugin-io');

function Plugin1(username) {
  this.user = username;
}

Plugin1.prototype.echo = function (what) {
  return Promise.resolve(what + ' ' + this.user);
};

Plugin1.prototype.onlyAdmin = function () {
  return Promise.resolve('hi there!');
};

require('async')
.autoInject(
  {
    privateKey: function (callback) {
      callback(null, '-----BEGIN RSA PRIVATE KEY-----\nMIIBPAIBAAJBANTuAVO3T8W0zvEMVvaeOrV8JOdXsIYndQhbQLO1OJ+/ZGvo8VrZ\nnVX5BtIOlWzhei9gEDL/pajDsF6BhrsXO+kCAwEAAQJBAM3HVjD5r3Z6TqRWMJUW\nRdauq1uIO2jrKQdyaQ1Dzf1SzlbmTAP2nSOn6J/TPUmQVpiOLwj3soUfXrRMbODi\njAECIQD27oHXpwgC13sMwf2mnX3vpq2o9bL5gm5DWXj5lHwIUQIhANy/1VpEaZNn\nSYLoV7HOVNPukREqc/pyYQ0ZCVKaJKwZAiEAoBk1cBeo1wbUjgn8phk4fLfpokFi\n/+i0CtCo4dCGtnECIQDWjTi/aDi4xK3FJy98qI74ASpL5dgddifvxAK0nw6/mQIg\nMwKOLnqLPRJ/IbqB9XTUSUWtdihdsaCGYbcO780siUM=\n-----END RSA PRIVATE KEY-----');
    }
  },
  function (err, results) {
    var pluginIO = new PluginIO(Plugin1, results.privateKey, {}, {
      'my-event': function (message) {
        console.log(message);
      }
    });
    
    pluginIO
    .connect('http://localhost:4000')
    .then(function () {
      console.log('connected');
    })
    .catch(function (err) {
      console.error(err);
      
      process.exit(-1);
    });
  }
);
